package controllers

import (
	"fmt"
	myAppv1 "github.com/huangxinzhong/huangxinzhong-deployment/api/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	networkv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
	"os"
	"reflect"
	"testing"
)

func readFile(fileName string) []byte {
	content, err := os.ReadFile(fmt.Sprintf("testdata/%s", fileName))
	if err != nil {
		panic(err)
	}
	return content
}

func newHxzDeployment(fileName string) *myAppv1.HxzDeployment {
	content := readFile(fileName)
	md := new(myAppv1.HxzDeployment)
	if err := yaml.Unmarshal(content, md); err != nil {
		panic(err)
	}
	return md
}

func newDeployment(fileName string) *appsv1.Deployment {
	content := readFile(fileName)
	md := new(appsv1.Deployment)
	if err := yaml.Unmarshal(content, md); err != nil {
		panic(err)
	}
	return md
}

func newService(fileName string) *corev1.Service {
	content := readFile(fileName)
	md := new(corev1.Service)
	if err := yaml.Unmarshal(content, md); err != nil {
		panic(err)
	}
	return md
}

func newIngress(fileName string) *networkv1.Ingress {
	content := readFile(fileName)
	md := new(networkv1.Ingress)
	if err := yaml.Unmarshal(content, md); err != nil {
		panic(err)
	}
	return md
}

func TestNewDeployment(t *testing.T) {
	type args struct {
		md *myAppv1.HxzDeployment
	}
	tests := []struct {
		name string             // 测试用例名称
		args args               // 测试函数的参数
		want *appsv1.Deployment // 期望结果
	}{
		{
			name: "测试使用 ingress mode 时候，生成 Deployment 资源",
			args: args{
				md: newHxzDeployment("hxz-ingress-cr.yaml"),
			},
			want: newDeployment("hxz-ingress-deployment-expect.yaml"),
		},
		{
			name: "测试使用 nodeport mode 时候，生成 Deployment 资源",
			args: args{
				md: newHxzDeployment("hxz-nodeport-cr.yaml"),
			},
			want: newDeployment("hxz-nodeport-deployment-expect.yaml"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewDeployment(tt.args.md)
			if !reflect.DeepEqual(got, *tt.want) {
				t.Errorf("NewDeployment() got = %v, want %v", got, *tt.want)
			}
		})
	}
}

func TestNewIngress(t *testing.T) {
	type args struct {
		md *myAppv1.HxzDeployment
	}
	tests := []struct {
		name string
		args args
		want *networkv1.Ingress
	}{
		{
			name: "测试使用 ingress mode 时候， 生成 ingress 资源",
			args: args{
				md: newHxzDeployment("hxz-ingress-cr.yaml"),
			},
			want: newIngress("hxz-ingress-ingress-expect.yaml"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewIngress(tt.args.md)
			if !reflect.DeepEqual(got, *tt.want) {
				t.Errorf("NewIngress() got = %v, want %v", got, *tt.want)
			}
		})
	}
}

func TestNewService(t *testing.T) {
	type args struct {
		md *myAppv1.HxzDeployment
	}
	tests := []struct {
		name string
		args args
		want *corev1.Service
	}{
		{
			name: "测试使用 ingress mode时候生成 service 资源",
			args: args{
				md: newHxzDeployment("hxz-ingress-cr.yaml"),
			},
			want: newService("hxz-ingress-service-expect.yaml"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewService(tt.args.md)
			if !reflect.DeepEqual(got, *tt.want) {
				t.Errorf("NewService() got = %v, want %v", got, *tt.want)
			}
		})
	}
}
